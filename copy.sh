export TINYUSB=../tinyusb

rsync -rR $(find ${TINYUSB}/src/./ -type f -name "*.c") ${TINYUSB}/hw/./bsp/board.c ${TINYUSB}/hw/./bsp/stm32l052k8/stm32l052k8.c ./src/
cp ${TINYUSB}/examples/device/cdc_msc/src/*.c ./src/

rsync -rR $(find ${TINYUSB}/src/./ -type f -name "*.h") ${TINYUSB}/hw/./bsp/*.h ./include/
cp ${TINYUSB}/examples/device/cdc_msc/src/*.h ./include/

sed -i'' 's$#include "../board.h"$#include "board.h"$' ./src/bsp/stm32l052k8/stm32l052k8.c
